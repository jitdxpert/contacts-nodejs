const express = require('express');
const router = express.Router();

const Contact = require('../models/contacts.model');

router.get('/contacts', (req, res, next) => {
  Contact.find((err, result) => {
    res.json(result);
  });
});

router.post('/contact', (req, res, next) => {
  let newContact = new Contact({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    phone: req.body.phone
  });
  newContact.save((err, result) => {
    if(err){
      res.json({
        code: 'error',
        message: 'Failed to add contact.'
      });
    }else{
      res.json({
        code: 'success',
        message: 'Contact added successfully.'
      });
    }
  })
});

router.delete('/contact/:id', (req, res, next) => {
  let id = {
    _id: req.params.id
  };
  Contact.remove(id, (err, result) => {
    if(err){
      res.json(err);
    }else{
      res.json(result);
    }
  })
});

module.exports = router;
