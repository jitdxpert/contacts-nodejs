const express = require('express');
const router = express.Router();

var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const User = require('../models/users.model');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');

var VerifyToken = require('./verify-token');

// NEW USER REGISTRATION
router.post('/register', (req, res) => {
  var hashedPassword = bcrypt.hashSync(req.body.password, 8);

  let first_name = req.body.first_name;
  let last_name = req.body.last_name;
  let email = req.body.email;
  let password = hashedPassword;
  let mpin = '';

  if(!first_name || !last_name || !email || !password) return res.status(500).send('Fields are required.');

  let newUser = new User({
    first_name: first_name,
    last_name: last_name,
    email: email,
    password: password,
    mpin: mpin
  });
  newUser.save((err, result) => {
    if(err) return res.status(500).send('There was a problem registering the user.');
    // create a token
    var token = jwt.sign({id: result._id}, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).json({
      auth: true,
      token: token,
      message: 'User added successfully.',
      data: result
    });
  });
});

// USER LOGIN
router.post('/login', (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  if(!email || !password) return res.status(500).send('Enter email & password.');

  let user = {
    email: email
  };
  User.findOne(user, (err, result) => {
    if(err) return res.status(500).send('Failed to login.');
    if(!result) return res.status(500).send('Invalid email & password.');

    var passwordIsValid = bcrypt.compareSync(password, result.password);
    if(!passwordIsValid)
      return res.status(401).send({
        auth: false,
        token: null
      });

    // create a token
    var token = jwt.sign({id: result._id}, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).json({
      auth: true,
      token: token,
      message: 'Successfully logged in.',
      data: result
    });
  });
});

// USER LOGIN USING MPIN
router.post('/mpin', (req, res) => {
  let mpin = req.body.mpin;

  if(!mpin) return res.status(500).send('PIN required.');

  let user = {
    mpin: mpin
  };
  User.findOne(user, (err, result) => {
    if(err) return res.status(500).send('Failed to login.');
    if(!result) return res.status(500).send('Invalid PIN.');

    res.status(200).json({
      message: 'Successfully logged in.',
      data: result
    });
  });
});

// CHECKING USER STATUS
router.get('/me', VerifyToken, (req, res, next) => {
  User.findById(req.userId, { password: 0 }, function (err, result) {
    if (err) return res.status(500).send("There was a problem finding the user.");
    if (!result) return res.status(404).send("No user found.");

    res.status(200).json({
      message: 'User authenticated.',
      data: result
    });
  });
});

// RETURN ALL USERS
router.get('/users', (req, res) => {
  User.find({}, (err, result) => {
    if(err) return res.status(500).send('There was a problem finding the users.');
    res.status(200).json({
      message: 'All users.',
      data: result
    });
  });
});

// GET SINGLE USER
router.get('user/:id', (req, res) => {
  User.findById(req.params.id, (err, result) => {
    if(err) return res.status(500).send('There was a problem finding the user.');
    if(!result) return res.status(404).send('No user found.');
    res.status(200).json({
      message: 'Return user.',
      data: result
    });
  });
});

// DELETE A USER
router.delete('user/:id', (req, res) => {
  User.findByIdAndRemove(req.params.id, (err, result) => {
    if(err) return res.status(500).send('There was a problem deleting the user.');
    res.status(200).json({
      message: 'User: '+result.name+' was deleted.',
      data: result
    });
  });
});

// UPDATE SINGLE USER
router.put('user/:id', (req, res) => {
  User.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, result) => {
    if(err) return res.status(500).send('There was a problem updating the user.');
    res.status(200).json({
      message: 'User: '+result.name+' was updated.',
      data: user
    });
  });
});

// USER LOGOUT
router.get('/logout', function(req, res) {
  res.status(200).send({
    auth: false,
    token: null,
    message: 'Successfully logged out.',
  });
});

// ADD MIDDLEWARE
router.use(function (result, req, res, next) {
  res.status(200).json(reset);
});

module.exports = router;
