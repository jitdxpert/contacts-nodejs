// importing modules
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

let app = express();

const route = require('./routes/route');
const auth = require('./routes/auth');

// connect to mongodb
mongoose.connect('mongodb://localhost:27017/contacts');
// on connection
mongoose.connection.on('connected', ()=>{
  console.log('Connected to database mongodb@27017');
});
// error connection
mongoose.connection.on('error', (err)=>{
  if(err){
    console.log('Error in database connection: '+err);
  }
});


// adding middleware
app.use(cors());                                          // cors for cross domain requests
app.use(bodyParser.json());                               // body-parser for parsing data from body

// set static folder for public assets
app.use(express.static(path.join(__dirname, 'public')));

// setting Rest API URLs
app.use('/api', route);
// setting Rest Auth URLs
app.use('/auth', auth);


// testing server
app.get('/', (req, res)=>{
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.get('/contacts', (req, res)=>{
  res.sendFile(path.join(__dirname, '/public/contacts.html'));
});


// starting server
let port = process.env.PORT || 4100;
app.listen(port,()=>{
  console.log('Server started at port:'+port);
});
